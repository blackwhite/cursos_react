import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from 'actions';
import requireAuth from 'components/requireAuth';

class CommentBox extends Component {

  state = { comment: ''};

  handleChange = event => {
    this.setState( { comment: event.target.value });
  };

  handleSubmit = event => {
    event.preventDefault();

    this.props.saveComment(this.state.comment);

    this.setState({ comment: ''});
  }

  render() {
    return (
      <div>
        <form onSubmit={this.handleSubmit.bind(this)}>
          <h4>Adicionar um comentário</h4>
          <textarea onChange={this.handleChange.bind(this)} value={this.state.comment} />
          <div>
            <button>Enviar comentário</button>
          </div>
        </form>
        <button className="fetch-comments" onClick={this.props.fetchComments}>Obter cometários</button>
      </div>
    );
  }
};


export default connect(null, actions)(requireAuth(CommentBox));
