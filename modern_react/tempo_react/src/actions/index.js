import axios from 'axios';

const API_KEY = 'ee1edd17da81d5d46ed29666b5c247a8';
const ROOT_URL = `http://api.openweathermap.org/data/2.5/forecast?appid=${API_KEY}`;

export const FETCH_WEATHER = 'FETCH_WEATHER';

export function fetchWeather(city) {
  const url = `${ROOT_URL}&q=${city},br`;
  const request = axios.get(url);

  console.log(request);

  return {
    type: FETCH_WEATHER,
    payload: request
  }
}
