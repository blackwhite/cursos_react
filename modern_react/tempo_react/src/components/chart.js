import React from 'react';
import { Sparklines, SparklinesLine, SparklinesReferenceLine} from 'react-sparklines';

function calcularMedia(dados) {
  const total = dados.reduce((a, b) => a+b);

  return Math.round(total / dados.length);
}

export default (props) => {
  return (
    <div>
      <Sparklines height={120} width={180} data={props.data}>
        <SparklinesLine color={props.color} />
        <SparklinesReferenceLine type="avg"/>
      </Sparklines>
      <div>{calcularMedia(props.data)} {props.units}</div>
    </div>
  );
}
