import React, {Component} from 'react';
import { Field, reduxForm } from 'redux-form';
import { Link } from 'react-router-dom';
import {connect} from 'react-redux';
import {createPost} from '../actions';


class PostsNew extends Component {

  renderField(field) {
    const {meta: {touched, error}} = field;
    const errorClass = 'has-danger';
    const className = `form-group ${touched && error ? errorClass:''}`;

    return (
      <div className={className}>
        <label>{field.label}</label>
        <input
          className="form-control"
          type="text"
          {...field.input}
        />
        <div className="text-help">
          {touched ? error : ''}
        </div>
      </div>
    );
  }

  onSubmit(values) {
    this.props.createPost(values, () => {
      this.props.history.push('/');
    });
  }

  render() {
    const { handleSubmit } = this.props;
    return (
      <div>
        <form onSubmit={handleSubmit(this.onSubmit.bind(this))}>
         <Field
            label="Título"
            name="title"
            component={this.renderField}
         />

         <Field
           label="Categorias"
           name="categories"
           component={this.renderField}
         />

         <Field
            label="Conteúdo"
            name="content"
            component={this.renderField}
         />
         <button type="submit" className="btn btn-primary">Enviar</button>
         <Link className="btn btn-danger" to="/">Cancelar</Link>
        </form>
      </div>
    );
  }
}

function validate(values) {
  const errors = {}

  if (!values.title) {
    errors.title = "Digite o título do artigo";
  }

  if (!values.categories) {
    errors.categories = "Digite o nome da categoria";
  }

  if (!values.content) {
    errors.content = "Digite o conteúdo do artigo";
  }

  return errors;
}

export default reduxForm({
  validate,
  form: 'PostsNewForm'
})(
  connect(null, {createPost})(PostsNew)
);
